# Curriculum Vitae #

![](https://virtual.ecaib.org/pluginfile.php/82892/user/icon/adaptable/f1?rev=1134963)

## Datos Personales ##

**nombre y apellidos**: jordi german quiñe dongo

**Dirección**: C/cristobal de moura 217

**teléfono**: 642730283

**Correo**: [jordi-qd@hotmail.com](jordi-qd@hotmail.com)

## Formación académica   ##

- **2012-2016 Graduado de educación secundaria obligatoria ESO**
- **2017-2019 Ciclo Formativo grado medio de Técnico en Sistemas
Microinformático y Redes.**

## Informática ##

- **Windows**
- **Linux**
- **Microsoft word, Write**
- **Excel,Calc**
- **Access,Bases**
- **Internet**
- **Power point,impress**

## Datos de interés ##

**Curso particular de catalán , disponibilidad a todas horas , persona
comprometida con el trabajo.**

